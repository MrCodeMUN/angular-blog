export class Post {
    id: number;
    title: string;
    content: string;
    loveIts: number;

    // La date du post ne peut être enregistrée dans une base de données Firebase en tant qu'objet Date.
    // Afin de pouvoir conserver la date du post, nous devons la convertir dans un format compris de Firebase (ici : nombre).
    created_at: number;

    constructor(title: string, content: string) {
        this.title = title;
        this.content = content;
        this.loveIts = 0;
        this.created_at = new Date().getTime();
    }
}