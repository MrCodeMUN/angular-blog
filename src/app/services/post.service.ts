import { Injectable } from '@angular/core';
import { Post } from '../models/post.model';
import { Subject } from 'rxjs';
import * as firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable({
  providedIn: 'root'
})
export class PostService {

  postSubject = new Subject<Post[]>();

  posts: Post[] = [];

  constructor() {
    this.getPosts();
  }

  emitPostSubject() {
    this.postSubject.next(this.posts.slice());
  }

  getPosts() {
    firebase.database().ref('/posts').on('value', (data: DataSnapshot) => {
      this.posts = data.val() ? data.val() : [];
      this.emitPostSubject();
    });
  }

  savePosts() {
    firebase.database().ref('/posts').set(this.posts);
    this.emitPostSubject();
  }

  createNewPost(title: string, content: string) {
    const post: Post = new Post(title, content);
    post.id = (this.posts.length - 1) + 1;
    this.posts.push(post);
    this.savePosts();
  }

  removePost(index: number) {
    this.posts.splice(index, 1);
    this.savePosts();
  }

  upvotePost(index: number) {
    this.posts[index].loveIts++;
    this.savePosts();
  }

  downvotePost(index: number) {
    this.posts[index].loveIts--;
    this.savePosts();
  }
}
