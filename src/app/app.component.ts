import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  constructor() {
    var firebaseConfig = {
      apiKey: "AIzaSyBWbU1nfMboMPmxFS8p7GzPLycul0-Rqp8",
      authDomain: "angular-blog-9a91c.firebaseapp.com",
      databaseURL: "https://angular-blog-9a91c.firebaseio.com",
      projectId: "angular-blog-9a91c",
      storageBucket: "angular-blog-9a91c.appspot.com",
      messagingSenderId: "170785642296",
      appId: "1:170785642296:web:7af76664e47c6ab91553cd"
    };

    firebase.initializeApp(firebaseConfig);
  }
}
